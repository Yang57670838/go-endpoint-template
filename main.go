package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/Yang57670838/go-endpoint-template/helpers"
)

type Client struct {
	FirstName   string
	LastName    string
	PhoneNumber string
	Age         int
}

type CustomerPayload struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
	Age         int    `json:"age"`
}

type Admin struct {
	adminID string
}

type Appuser interface {
	Login() string
	Logout() string
}

type Vertex struct {
	X, Y float64
}

type Rect struct {
	width  int
	height int
}

type Dog struct {
	Breed  string
	Weight int16
}

func (d Dog) getbreed() string {
	return d.Breed
}

func (r *Rect) area() int {
	return r.width * r.height
}

func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func (u *Client) printFirstname() string {
	return u.FirstName
}

func calculateValue(c chan int) {
	randomeNumber := helpers.RandomNumber(10)
	c <- randomeNumber
}

const portNumber = ":4000"

func main() {
	dog := Dog{Breed: "Dalmation", Weight: 40}
	fmt.Println("dog's breed is: ", dog.getbreed())

	myR := &Rect{width: 10, height: 5}
	fmt.Println("area: ", myR.area())

	v := &Vertex{3, 4}
	v.Scale(5)

	var testCustomer helpers.Customer
	testCustomer.Name = "Jerry"

	c := make(chan int)
	defer close(c)
	go calculateValue(c)

	num := <-c
	log.Println(num)

	var userSlice []Client
	testuser := Client{
		FirstName: "Yang",
		LastName:  "Liu",
	}
	testuser2 := Client{
		FirstName: "Tom",
		LastName:  "Cruise",
	}
	testadmin := Admin{
		adminID: "12345",
	}

	PrintInfo(testuser)
	PrintInfo(testadmin)
	LoginApi(testuser)

	userSlice = append(userSlice, testuser)
	userSlice = append(userSlice, testuser2)
	for _, x := range userSlice {
		log.Println(x.FirstName)
	}

	var myString string

	myString, _ = saySomething("hello")

	log.Println("myString is set to", myString)

	changeUsingPointers(&myString)
	log.Println("myString is set to", myString)

	mockCustomersPayload := `
	[
		{
			"first_name": "Yang",
			"last_name": "Liu",
			"phone_number": "0425939393",
			"age": 30
		},
		{
			"first_name": "Tom",
			"last_name": "Cruis",
			"phone_number": "0425939393",
			"age": 50
		}
	]`

	//decode JSON string
	var unmarshalled []CustomerPayload

	err := json.Unmarshal([]byte(mockCustomersPayload), &unmarshalled)
	if err != nil {
		log.Println("Error unmarshalling json", err)
	}
	log.Printf("unmarshalled: %v", unmarshalled)

	//encode struct to JSON
	var mySlice []CustomerPayload
	var c1 CustomerPayload
	c1.Age = 10
	c1.FirstName = "Test"
	c1.LastName = "TEST"
	c1.PhoneNumber = "0524293854"
	mySlice = append(mySlice, c1)

	var c2 CustomerPayload
	c2.Age = 20
	c2.FirstName = "David"
	c2.LastName = "Jones"
	c2.PhoneNumber = "0293739493"
	mySlice = append(mySlice, c2)

	newJSON, err := json.Marshal(mySlice)
	if err != nil {
		log.Println("error marshalling", err)
	}
	fmt.Println(string(newJSON))

	result, err := divide(100.0, 10.0)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("result of division is", result)

	// http
	srv := &http.Server{
		Addr:    portNumber,
		Handler: routes(),
	}
	err = srv.ListenAndServe()
	log.Fatal(err)
}

func saySomething(s string) (string, string) {
	return s, "world"
}

func changeUsingPointers(s *string) {
	log.Println("s is set to", s)
	newValue := "Red"
	*s = newValue
}

func (u Client) Login() string {
	return u.FirstName + " client login"
}

func (u Client) Logout() string {
	return u.FirstName + " client Logout"
}

func (a Admin) Login() string {
	return a.adminID + " admin login"
}

func (a Admin) Logout() string {
	return a.adminID + " admin Logout"
}

func PrintInfo(user Appuser) {
	log.Println("this user ", user.Login(), user.Logout())
}

func LoginApi(user Appuser) {
	log.Println(user.Login())
}

func divide(x, y float32) (float32, error) {
	var result float32

	if y == 0 {
		return result, errors.New("cannot divide by 0")
	}

	result = x / y
	return result, nil
}
