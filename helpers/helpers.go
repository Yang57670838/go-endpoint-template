package helpers

import (
	"math/rand"
	"time"
)

type Customer struct {
	Name  string
	Phone string
}

func RandomNumber(n int) int {
	rand.Seed(time.Now().UnixNano())
	value := rand.Intn(n)
	return value
}
