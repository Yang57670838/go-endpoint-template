package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"gitlab.com/Yang57670838/go-endpoint-template/pkg/handlers"
)

func routes() http.Handler {
	mux := chi.NewRouter()

	mux.Use(middleware.Recoverer)
	mux.Use(writeToConsole)

	mux.Get("/", http.HandlerFunc(handlers.Home))

	return mux
}
